package sparkninjas.aws;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;

import java.io.File;

/**
 * Example downloading a file from Amazon S3
 */
public class S3DownloadFile
{
    // Set these to the correct values for your AWS account
    static String ACCESS_KEY = "AKXXXXXXXXXXXXXXX";
    static String ACCESS_SECRET = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

    public static void main( String[] args )
    {
        // Initialise the AmazonS3Client
        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(ACCESS_KEY, ACCESS_SECRET));

        // Set the details of the S3 object we want
        String bucketName = "mybucket";
        String fileName = "path/to/my/file.txt";
        GetObjectRequest request = new GetObjectRequest(bucketName, fileName);

        // Set the local file where we want to store it
        File file = new File("file.txt");

        // Download the S3 object to the given local file
        s3Client.getObject(request, file);
    }
}
